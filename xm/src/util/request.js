import axios from 'axios'
import qs from 'qs'
import {getSign} from './sign'



const service = axios.create({
  baseURL: "http://49.234.178.198:8085", // api 的 base_url
  transformRequest: [function(data) {
    data = qs.stringify(data)
    return data
  }],
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  withCredentials: true,
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    // 添加通用请求参数
    var requestData = Object.assign({}, config.data)
    requestData.timestamp = new Date().getTime()
    requestData.appkey = "uy131pffafsfs"
    requestData.sign = getSign(requestData)
    config.data = requestData
    return config
  },
  error => {
    // Do something with request error
    Promise.reject(error)
  }
)

export default service
