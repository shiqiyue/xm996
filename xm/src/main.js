import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(VueRouter)
Vue.use(Element, {
    size: 'medium'
})

const router = new VueRouter({
    routes: [
        // 动态路径参数 以冒号开头
        {path: '/add', component: () => import('./add')},
        {path: '/list', component: () => import("./list")},
        {path: '/manage', component: () => import("./manage")},
        {path: '/login', component: () => import("./login")}
    ]
})

Vue.config.productionTip = false
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
