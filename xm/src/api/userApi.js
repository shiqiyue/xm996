import request from '../util/request'

function checkLogin(data){
    return request({
        url: '/api/user/checkLogin',
        method: 'post',
        data: data
    })
}

function login(data) {
    return request({
        url: '/api/user/login',
        method: 'post',
        data: data
    })
}

export default {
    checkLogin,
    login
}
