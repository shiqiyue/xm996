import request from '../util/request'

function getConfig(data){
    return request({
        url: '/api/config/get',
        method: 'post',
        data: data
    })
}

function setConfig(data) {
    return request({
        url: '/api/config/set',
        method: 'post',
        data: data
    })
}

export default {
    getConfig,
    setConfig
}
