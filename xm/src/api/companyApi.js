import request from '../util/request'

function add(data){
    return request({
        url: '/api/company/add',
        method: 'post',
        data: data
    })
}

function page(data) {
    return request({
        url: '/api/company/page',
        method: 'post',
        data: data
    })
}

function managePage(data){
    return request({
        url: '/api/company/manage/page',
        method: 'post',
        data: data
    })
}

function auditPass(data){
    return request({
        url: '/api/company/audit/pass',
        method: 'post',
        data: data
    })
}

function auditNotValid(data){
    return request({
        url: '/api/company/audit/notValid',
        method: 'post',
        data: data
    })
}

export default {
    page,
    add,
    managePage,
    auditNotValid,
    auditPass
}
