package com.github.shiqiyue.xm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.github.shiqiyue.xm.enums.CompanyStatus;
import lombok.Data;


import java.util.Date;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/9
 */
@Data
@TableName("xm_company")
public class Company {

    private Long id;

    private String name;

    private String reason;

    private String others;

    private Date addDate;

    private CompanyStatus status;


}
