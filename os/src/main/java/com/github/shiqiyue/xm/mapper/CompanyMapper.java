package com.github.shiqiyue.xm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.shiqiyue.xm.entity.Company;

public interface CompanyMapper extends BaseMapper<Company> {
}
