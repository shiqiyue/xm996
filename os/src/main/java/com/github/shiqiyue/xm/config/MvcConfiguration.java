package com.github.shiqiyue.xm.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.github.shiqiyue.xm.filter.SecurityFilter;
import com.github.shiqiyue.xm.filter.SignFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/***
 * spring mvc 配置
 * @author wenyao.wu
 * @date 2018/10/9
 */
@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Autowired
    private ApplicationConfig applicationConfig;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor();
        authenticationInterceptor.setConfig(securityConfig);
        registry.addInterceptor(authenticationInterceptor).addPathPatterns("/**");*/
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        // 配置序列化策略
        fastJsonConfig.setSerializerFeatures(SerializerFeature.BrowserCompatible);
        converter.setFastJsonConfig(fastJsonConfig);
        converters.add(0, converter);
    }


    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        for (String origin : applicationConfig.getAllowOrigins()) {
            config.addAllowedOrigin(origin);
        }
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.addExposedHeader("X-Auth-Token");
        // CORS 配置对所有接口都有效
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(-2000);
        return bean;
    }

    @Bean FilterRegistrationBean securityFilter(){
        SecurityFilter securityFilter = new SecurityFilter();
        for(String path: applicationConfig.getSecurityPaths()){
            securityFilter.addSecurePath(path);
        }
        FilterRegistrationBean bean = new FilterRegistrationBean(securityFilter);
        bean.setOrder(-1);
        return bean;
    }

    @Bean
    public FilterRegistrationBean signFilter(){
        SignFilter signFilter = new SignFilter();
        signFilter.setAppkeyStore(new SignFilter.AppKeyStore() {
            @Override
            public String getAppSecretByAppkey(String appkey) {
                if("uy131pffafsfs".equals(appkey)){
                    return "3213123dasda13123543fdfdas";
                }
                return null;
            }
        });
        signFilter.addSecurePath("/api/**");
        FilterRegistrationBean bean = new FilterRegistrationBean(signFilter);
        bean.setOrder(-200);
        return bean;
    }


}
