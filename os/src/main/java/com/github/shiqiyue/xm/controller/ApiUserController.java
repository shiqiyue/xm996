package com.github.shiqiyue.xm.controller;

import com.github.shiqiyue.xm.common.BaseApiController;
import com.github.shiqiyue.xm.common.Result;
import com.github.shiqiyue.xm.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/***
 *
 * @author wenyao.wu
 * @date:2019/8/25
 */
@RestController
@RequestMapping("/api/user")
public class ApiUserController extends BaseApiController {

    @Autowired
    private ApplicationConfig applicationConfig;

    @PostMapping("checkLogin")
    public Result checkLogin(HttpSession httpSession){
        if(Boolean.TRUE.equals(httpSession.getAttribute("login"))){
            return Result.success();
        }
        return Result.fail("未登录");
    }

    @PostMapping("login")
    public Result login(String username, String password, HttpSession httpSession) {
        if (applicationConfig.getUsername().equals(username) && applicationConfig.getPassword().equals(password)) {
            httpSession.setAttribute("login", true);
            return Result.success();
        }
        return Result.fail("登录失败");
    }

}
