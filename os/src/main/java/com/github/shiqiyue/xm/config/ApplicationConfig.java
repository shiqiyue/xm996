package com.github.shiqiyue.xm.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/***
 * 应用程序配置信息
 * @author wenyao.wu
 * @date 2019/1/8
 */
@Data
@Component
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {



    /***
     * origin to allow.
     */
    private List<String> allowOrigins = new ArrayList<>();


    private String username;

    private String password;

    private List<String> securityPaths = new ArrayList<>();

}
