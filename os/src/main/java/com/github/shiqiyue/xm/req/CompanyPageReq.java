package com.github.shiqiyue.xm.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/9
 */
@Data
public class CompanyPageReq {

    @NotNull
    private Integer current;

    @NotNull
    private Integer size;

    private String name;


}
