package com.github.shiqiyue.xm.common;

import com.baomidou.mybatisplus.core.enums.IEnum;

/***
 * 基础枚举类
 * @author wenyao.wu
 * @date 2019/7/17
 */
public interface IBaseEnum extends IEnum<Integer> {

    /***
     * 获取名称
     * @return
     */
    public String getName();
}
