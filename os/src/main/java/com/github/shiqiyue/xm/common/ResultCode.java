package com.github.shiqiyue.xm.common;

/***
 *
 * @author wenyao.wu
 * @date 2018/10/8
 */
public class ResultCode {


    /***
     * 成功
     */
    public static final Integer COMMON_SUCCESS = 200;


    /***
     * 一般错误
     */
    public static final Integer COMMON_FAIL = 400;

    /***
     * 权限不足
     */
    public static final Integer PERMISSION_DENY = 403;

    /***
     * 未登陆
     */
    public static final Integer NOT_LOGIN = 600;
}
