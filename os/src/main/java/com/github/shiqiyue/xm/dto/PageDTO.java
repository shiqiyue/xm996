package com.github.shiqiyue.xm.dto;

import lombok.Data;

import java.util.List;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/10
 */
@Data
public class PageDTO<T> {
    private Integer current;

    private Integer size;

    private Long total;

    private List<T> records;

}
