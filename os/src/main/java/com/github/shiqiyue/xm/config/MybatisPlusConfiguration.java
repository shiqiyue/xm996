package com.github.shiqiyue.xm.config;

/***
 * mybatis plus 配置类
 * @author wenyao.wu
 * @date 2018/10/11
 */

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/***
 * mybatis plus 配置
 * @author wwy
 */
@Configuration
@MapperScan({"com.github.shiqiyue.xm.mapper",})
public class MybatisPlusConfiguration {



    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }




}
