package com.github.shiqiyue.xm.req;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/9
 */
@Data
public class CompanyAddReq {

    @NotEmpty
    @Size(max = 50)
    private String name;

    @NotEmpty
    @Size(max = 400)
    private String reason;

    @NotEmpty
    @Size(max = 400)
    private String others;
}
