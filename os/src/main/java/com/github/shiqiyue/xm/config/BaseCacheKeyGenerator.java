package com.github.shiqiyue.xm.config;

import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

/***
 *
 * @author wenyao.wu
 * @date:2019/8/22
 */
public class BaseCacheKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object target, Method method, Object... params) {
        Object key = new BaseCacheKey(target, method, params);
        return key.toString();
    }

}