package com.github.shiqiyue.xm.enums;

import com.github.shiqiyue.xm.common.IBaseEnum;

/***
 *
 * @author wenyao.wu
 * @date:2019/8/22
 */
public enum  CompanyStatus implements IBaseEnum{


    VALID("有效",1),

    WAIT_AUDIT("待审核", 2),

    NOT_VALID("无效",3);

    private String name;

    private Integer value;
    ;

    CompanyStatus(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }


}
