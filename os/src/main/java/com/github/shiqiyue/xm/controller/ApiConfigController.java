package com.github.shiqiyue.xm.controller;

import com.github.shiqiyue.xm.common.Result;
import com.github.shiqiyue.xm.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 *
 * @author wenyao.wu
 * @date:2019/8/25
 */
@RestController
@RequestMapping("/api/config")
public class ApiConfigController {

    @Autowired
    private ConfigService configService;

    @PostMapping("get")
    public Result get(String key){
        return Result.success(configService.getValueByKey(key));
    }

    @PostMapping("set")
    public Result set( String key, String value){
        configService.setConfig(key, value);
        return Result.success();
    }
}
