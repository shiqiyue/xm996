package com.github.shiqiyue.xm.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/9
 */
@Slf4j
public class BaseApiController {

    @ExceptionHandler(Exception.class)
    public Result exceptionHandler(Exception e){
        log.error("异常:",e);
        return Result.fail("系统繁忙，请稍后再试");
    }

}
