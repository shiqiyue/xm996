package com.github.shiqiyue.xm.common;


import lombok.Data;

/***
 * 返回结果
 * @author wenyao.wu
 * @date 2018/10/8
 */
@Data
public class Result<T> {

    private T data;

    private Integer code;

    private String msg;

    public static <T> Result<T> success() {
        return success(null);
    }

    public static <T> Result<T> success(T data) {
        return newInstance(ResultMsg.COMMON_SUCCESS, ResultCode.COMMON_SUCCESS, data);
    }

    public static <T> Result<T> fail(String msg) {
        return newInstance(msg, ResultCode.COMMON_FAIL);
    }

    public static <T> Result<T> newInstance(String msg, Integer code) {
        return newInstance(msg, code, null);
    }

    public static <T> Result<T> newInstance(String msg, Integer code, T data) {
        Result<T> baseVO = new Result<>();
        baseVO.setData(data);
        baseVO.setCode(code);
        baseVO.setMsg(msg);
        return baseVO;
    }


}
