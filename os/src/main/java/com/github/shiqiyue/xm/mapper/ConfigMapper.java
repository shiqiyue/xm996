package com.github.shiqiyue.xm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.shiqiyue.xm.entity.Company;
import com.github.shiqiyue.xm.entity.Config;

public interface ConfigMapper extends BaseMapper<Config> {
}
