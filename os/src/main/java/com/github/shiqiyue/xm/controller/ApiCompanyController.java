package com.github.shiqiyue.xm.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.shiqiyue.xm.common.BaseApiController;
import com.github.shiqiyue.xm.common.Result;
import com.github.shiqiyue.xm.dto.PageDTO;
import com.github.shiqiyue.xm.entity.Company;
import com.github.shiqiyue.xm.enums.CompanyStatus;
import com.github.shiqiyue.xm.req.CompanyAddReq;
import com.github.shiqiyue.xm.req.CompanyPageReq;
import com.github.shiqiyue.xm.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

/***
 *
 * @author wenyao.wu
 * @date 2019/8/9
 */
@RestController
@RequestMapping("/api/company")
public class ApiCompanyController extends BaseApiController {

    @Autowired
    private CompanyService companyService;

    @PostMapping("add")
    public Result add(@Valid CompanyAddReq companyAddReq, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Result.fail(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        Company company = new Company();
        company.setAddDate(new Date());
        company.setOthers(companyAddReq.getOthers());
        company.setReason(companyAddReq.getReason());
        company.setName(companyAddReq.getName().trim());
        companyService.add(company);
        return Result.success();
    }

    @PostMapping("page")
    public Result page(@Valid CompanyPageReq companyPageReq, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Result.fail(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        IPage<Company> page = companyService.page(companyPageReq.getCurrent(), companyPageReq.getSize(), "");
        PageDTO pageDTO = new PageDTO();
        pageDTO.setCurrent(companyPageReq.getCurrent());
        pageDTO.setSize(companyPageReq.getSize());
        pageDTO.setTotal(page.getTotal());
        pageDTO.setRecords(page.getRecords());
        return Result.success(pageDTO);
    }

    @PostMapping("manage/page")
    public Result auditPage(@Valid CompanyPageReq companyPageReq, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Result.fail(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        IPage<Company> page = companyService.managePage(companyPageReq.getCurrent(), companyPageReq.getSize(), "");
        PageDTO pageDTO = new PageDTO();
        pageDTO.setCurrent(companyPageReq.getCurrent());
        pageDTO.setSize(companyPageReq.getSize());
        pageDTO.setTotal(page.getTotal());
        pageDTO.setRecords(page.getRecords());
        return Result.success(pageDTO);
    }

    @PostMapping("audit/pass")
    public Result auditPass(Long id){
        companyService.updateStatus(id, CompanyStatus.VALID);
        return Result.success();
    }

    @PostMapping("audit/notValid")
    public Result auditNotValid(Long id){
        companyService.updateStatus(id, CompanyStatus.NOT_VALID);
        return Result.success();
    }

}
