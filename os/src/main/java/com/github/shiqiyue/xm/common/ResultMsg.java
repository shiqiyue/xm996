package com.github.shiqiyue.xm.common;

/***
 * 接口返回信息
 * @author wenyao.wu
 * @date 2018/10/8
 */
public class ResultMsg {

    /***
     * 成功
     */
    public static final String COMMON_SUCCESS = "success";

    /***
     * 用户名或者密码错误
     */
    public static final String LOGIN_USERNAME_PASSWORD_ERROR = "用户名或者密码错误";

    /***
     * 该用户已被禁止登录
     */
    public static final String USER_DISABLED = "该用户已被禁止登录";

    /***
     * 添加失败
     */
    public static final String ADD_FAIL = "添加失败";

    /***
     * 修改失败
     */
    public static final String EDIT_FAIL = "修改失败";

    /***
     * 角色编码已存在
     */
    public static final String ROLE_CODE_EXIST = "角色编号已存在";

    /***
     * 当前用户信息不存在
     */
    public static final String CURRENT_USER_NOT_EXIST = "用户信息不存在";

    /***
     * 不能访问
     */
    public static final String CAN_NOT_ACCESS = "不能访问";

    /***
     * 请求失败
     */
    public static final String REQUEST_ERROR = "请求失败";
}
