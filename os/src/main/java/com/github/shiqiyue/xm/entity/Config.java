package com.github.shiqiyue.xm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/***
 * 配置项
 * @author wenyao.wu
 * @date:2019/8/22
 */
@Data
@TableName("xm_config")
public class Config {


    private Long id;

    private String configKey;

    private String configValue;


}
