package com.github.shiqiyue.xm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.shiqiyue.xm.entity.Config;
import com.github.shiqiyue.xm.mapper.ConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/***
 *
 * @author wenyao.wu
 * @date:2019/8/22
 */
@Service
public class ConfigService {

    @Autowired
    private ConfigMapper configMapper;

    //@Cacheable(cacheNames = "config")
    public List<Config> getAll(){
        return configMapper.selectList(new QueryWrapper<>());
    }


    //@CacheEvict(cacheNames = "config", allEntries = true)
    public void setConfig(String key, String value){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("config_key", key);
        Config config = configMapper.selectOne(queryWrapper);
        if(config == null){
            return;
        }
        config.setConfigValue(value);
        configMapper.updateById(config);
    }

    //@Cacheable(cacheNames = "config")
    public String getValueByKey(String key){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("config_key", key);
        Config config = configMapper.selectOne(queryWrapper);
        if(config == null){
            return null;
        }
        return config.getConfigValue();
    }

}
