package com.github.shiqiyue.xm;

import com.alibaba.fastjson.JSON;
import com.github.shiqiyue.xm.entity.Company;
import com.github.shiqiyue.xm.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyServiceTest {

    @Autowired
    private CompanyService companyService;

    @Test
    public void save() {
        Company company = new Company();
        company.setName("name");
        company.setAddDate(new Date());
        company.setReason("reason");
        company.setOthers("remark");
        companyService.add(company);
    }



}
